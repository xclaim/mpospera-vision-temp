package id.co.telkomsigma.btpns.mprospera.util;

import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("ALL")
public class CronUtil {

    private final Date mDate;
    private final Calendar mCal;
    private final String mSeconds = "0";
    private final String mDaysOfWeek = "?";
    private String mMins;
    private String mHours;
    private String mDaysOfMonth;
    private String mMonths;
    private String mYears;

    public CronUtil(Date pDate) {
        this.mDate = pDate;
        mCal = Calendar.getInstance();
    }

    public String getCron() {
        mCal.setTime(mDate);

        this.mHours = String.valueOf(mCal.get(Calendar.HOUR_OF_DAY));

        this.mMins = String.valueOf(mCal.get(Calendar.MINUTE));

        this.mDaysOfMonth = String.valueOf(mCal.get(Calendar.DAY_OF_MONTH));

        this.mMonths = new java.text.SimpleDateFormat("MM").format(mCal.getTime());

        this.mYears = String.valueOf(mCal.get(Calendar.YEAR));

        return this.mSeconds + " " + this.mMins + " " + this.mHours + " " + this.mDaysOfMonth + " " + this.mMonths + " "
                + this.mDaysOfWeek + " " + this.mYears;
    }

    public Date getDate() {
        return mDate;
    }

    public String getSeconds() {
        return mSeconds;
    }

    public String getMinutes() {
        return mMins;
    }

    public String getDaysOfWeek() {
        return mDaysOfWeek;
    }

    public String getHours() {
        return mHours;
    }

    public String getDaysOfMonth() {
        return mDaysOfMonth;
    }

    public String getMonths() {
        return mMonths;
    }

    public String getYears() {
        return mYears;
    }

    public void setMinutes(String mMins) {
        this.mMins = mMins;
    }

    public void setHours(String mHours) {
        this.mHours = mHours;
    }

    public void setDaysOfMonth(String mDaysOfMonth) {
        this.mDaysOfMonth = mDaysOfMonth;
    }

    public void setMonths(String mMonths) {
        this.mMonths = mMonths;
    }

    public void setYears(String mYears) {
        this.mYears = mYears;
    }

}