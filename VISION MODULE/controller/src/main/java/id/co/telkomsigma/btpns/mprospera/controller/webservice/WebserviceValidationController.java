package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;

@RestController
public class WebserviceValidationController extends GenericController {

    @Autowired
    private WSValidationService wsValidationService;

    @RequestMapping(value = WebGuiConstant.INTERNAL_WSVALIDATION, method = {RequestMethod.POST})
    public String wsValidation(@RequestParam String username, @RequestParam String imei,
                               @RequestParam String sessionKey, @RequestParam String apkVersion) {
        if (imei == null || sessionKey == null || username == null || apkVersion == null) {
            return "XX";
        }
        return wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
    }

    @RequestMapping(value = WebGuiConstant.INTERNAL_LOGINVALIDATION, method = {RequestMethod.POST})
    public String loginValidation(@RequestParam String username, @RequestParam String imei,
                                  @RequestParam String apkVersion) {
        return wsValidationService.loginValidation(username, imei, apkVersion);
    }

}