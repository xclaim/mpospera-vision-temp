package id.co.telkomsigma.btpns.mprospera.controller.configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("local")
@EnableConfigurationProperties({DataSourceProperties.class})
public class UrlDatabaseConfig {

    protected Log log = LogFactory.getLog(this.getClass());

//	@Bean(name = "primaryDataSource")
//	@Qualifier("primaryDataSource")
//	@Primary
//	@ConfigurationProperties(prefix = "spring.datasource")
//	@ConditionalOnProperty(prefix = "spring.datasource", name = { "url" })
//	public DataSource primary() {
//		return DataSourceBuilder.create().build();
//	}

}