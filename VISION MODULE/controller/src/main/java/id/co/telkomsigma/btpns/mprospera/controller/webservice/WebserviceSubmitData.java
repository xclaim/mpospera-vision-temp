package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.response.BaseResponse;
import id.co.telkomsigma.btpns.mprospera.response.SubmitData;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//SUBMIT MOBILE DATA API
@Controller("webserviceSubmitData")
public class WebserviceSubmitData extends GenericController {

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private UserService userServiceWithCache;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    final JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_SUBMIT_TERMINAL_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    BaseResponse submitTerminalData(@RequestBody final String data,
                                    @PathVariable("apkVersion") String apkVersion) throws Exception {

        final BaseResponse responseCode = new BaseResponse();
        try {
            log.info("INCOMING SUBMIT TERMINAL DATA MESSAGE : " + jsonUtils.toJson(data));
            Object obj = new JsonUtils().fromJson(data, SubmitData.class);
            final SubmitData request = (SubmitData) obj;
            responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
            try {
                String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                        request.getSessionKey(), apkVersion);
                if (!validation.equals("00")) {
                    responseCode.setResponseCode(validation);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    log.error("VALIDATION FAILED");
                    log.info("RESPONSE MESSAGE : " + responseCode.toString());
                    return responseCode;
                } else {
                    terminalActivityService.submitTerminalData(request.getTransmissionDateAndTime(),
                            request.getRetrievalReferenceNumber(), request.getUsername(), request.getSessionKey(),
                            request.getImei(), request.getProcessor(), request.getRam(), request.getGpsVersion(),
                            request.getTotalDeviceMemory(), request.getFreeDeviceMemory(), request.getOperatorName(),
                            request.getSignal(), apkVersion);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            }
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            threadPoolTaskExecutor.execute(new Runnable() {

                @Override
                public void run() {
                    Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                    TerminalActivity terminalActivity = new TerminalActivity();
                    // Logging to terminal activity
                    terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                    terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_ANDROID_DATA);
                    terminalActivity.setCreatedDate(new Date());
                    terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                    terminalActivity.setSessionKey(request.getSessionKey());
                    terminalActivity.setTerminal(terminal);
                    terminalActivity.setUsername(request.getUsername().trim());
                    // save message masuk
                    MessageLogs incoming = new MessageLogs();
                    incoming.setCreatedDate(new Date());
                    incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                    try {
                        incoming.setMessageRaw(new JsonUtils().toJson(request).getBytes());
                    } catch (JsonMappingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (JsonGenerationException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    incoming.setRequest(true);
                    incoming.setTerminalActivity(terminalActivity);
                    // save message keluar
                    MessageLogs outgoing = new MessageLogs();
                    outgoing.setCreatedDate(new Date());
                    outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                    try {
                        outgoing.setMessageRaw(new JsonUtils().toJson(responseCode).getBytes());
                    } catch (JsonMappingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (JsonGenerationException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    outgoing.setRequest(false);
                    outgoing.setTerminalActivity(terminalActivity);
                    List<MessageLogs> messageLogs = new ArrayList<>();
                    messageLogs.add(incoming);
                    messageLogs.add(outgoing);
                    terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                }
            });
        } catch (Exception e) {
            log.error(e.getMessage(), e);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(data);
            String transmissionDateTime = rootNode.path("transmissionDateAndTime").textValue();
            final String retrievalReferenceNumber = rootNode.path("retrievalReferenceNumber").textValue();
            final String user = rootNode.path("username").textValue();
            final String sessionKey = rootNode.path("sessionKey").textValue();
            final String imei = rootNode.path("imei").textValue();
            try {
                String processor = rootNode.path("processor").textValue();
                String ram = rootNode.path("ram").textValue();
                String gpsVersion = rootNode.path("gpsVersion").textValue();
                String totalDeviceMemory = rootNode.path("totalDeviceMemory").textValue();
                String freeDeviceMemory = rootNode.path("freeDeviceMemory").textValue();
                String operatorName = rootNode.path("operatorName").textValue();
                String signal = rootNode.path("signal").textValue();
                terminalActivityService.submitTerminalData(transmissionDateTime, retrievalReferenceNumber, user,
                        sessionKey, imei, processor, ram, gpsVersion, totalDeviceMemory, freeDeviceMemory, operatorName,
                        signal, apkVersion);
                responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
            } catch (Exception z) {
                log.error("ERROR.....Failed to Submit Terminal Data : " + z.getMessage());
                responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
            } finally {
                try {
                    log.info("Trying to Create Terminal Activity and Message Logs");
                    threadPoolTaskExecutor.execute(new Runnable() {

                        @Override
                        public void run() {
                            Terminal terminal = terminalService.loadTerminalByImei(imei);
                            TerminalActivity terminalActivity = new TerminalActivity();
                            // Logging to terminal activity
                            terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                            terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SUBMIT_ANDROID_DATA);
                            terminalActivity.setCreatedDate(new Date());
                            terminalActivity.setReffNo(retrievalReferenceNumber.trim());
                            terminalActivity.setSessionKey(sessionKey);
                            terminalActivity.setTerminal(terminal);
                            terminalActivity.setUsername(user.trim());
                            // save message masuk
                            MessageLogs incoming = new MessageLogs();
                            incoming.setCreatedDate(new Date());
                            incoming.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                            try {
                                incoming.setMessageRaw(new JsonUtils().toJson(data).getBytes());
                            } catch (JsonMappingException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (JsonGenerationException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            incoming.setRequest(true);
                            incoming.setTerminalActivity(terminalActivity);
                            // save message keluar
                            MessageLogs outgoing = new MessageLogs();
                            outgoing.setCreatedDate(new Date());
                            outgoing.setEndpointCode(WebGuiConstant.ENDPOINT_MPROSPERA);
                            try {
                                outgoing.setMessageRaw(new JsonUtils().toJson(responseCode).getBytes());
                            } catch (JsonMappingException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (JsonGenerationException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            outgoing.setRequest(false);
                            outgoing.setTerminalActivity(terminalActivity);
                            List<MessageLogs> messageLogs = new ArrayList<>();
                            messageLogs.add(incoming);
                            messageLogs.add(outgoing);
                            log.info("Trying to SAVE Terminal Activity And Message Logs");
                            terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        }
                    });
                } catch (Exception x) {
                    log.error("Failed To SAVE Terminal Activity and Message Logs" + x.getMessage());
                }
            }
        }
        log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
        return responseCode;
    }

}