package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.ESBTokenRequest;
import id.co.telkomsigma.btpns.mprospera.request.TokenRequest;
import id.co.telkomsigma.btpns.mprospera.response.ESBTokenResponse;
import id.co.telkomsigma.btpns.mprospera.response.TokenResponse;
import id.co.telkomsigma.btpns.mprospera.service.ProsperaEncryptionService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class WebServiceTokenRequestController extends GenericController {

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    ProsperaEncryptionService prosperaEncryptionService;

    @Autowired
    private RESTClient restClient;

    final JsonUtils jsonUtils = new JsonUtils();

    // TOKEN REQUEST API FOR RESET PASSWORD USER LOGIN M-PROSPERA
    @RequestMapping(value = WebGuiConstant.TOKEN_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    TokenResponse doRequest(@RequestBody TokenRequest request,
                            @PathParam("apkVersion") String versionNumber) throws IOException {

        log.info("INCOMING REQUEST TOKEN MESSAGE : " + jsonUtils.toJson(request));
        TokenResponse response = new TokenResponse();
        Terminal terminal;
        try {
            // Create terminal
            if (terminalService.loadTerminalByImei(request.getImei().trim()) == null) {
                log.error("TERMINAL NOT FOUND...");
                response.setResponseCode("01");
                response.setResponseMessage("Unknown terminal");
                return response;
            } else {
                log.info("TERMINAL FOUND");
                terminal = terminalService.loadTerminalByImei(request.getImei().trim());
            }
            // create terminal activity record
            TerminalActivity terminalActivity = new TerminalActivity();
            // Logging to terminal activity
            terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
            terminalActivity.setActivityType(TerminalActivity.ACTIVITY_TOKEN_REQUEST);
            terminalActivity.setCreatedDate(new Date());
            terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
            terminalActivity.setSessionKey(request.getSessionKey());
            terminalActivity.setTerminal(terminal);
            terminalActivity.setUsername(request.getUsername().trim());
            log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
            // Logging to message logs incoming message from android
            List<MessageLogs> messageLogsList = new ArrayList<>();
            MessageLogs messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("MPROSPERA");
            messageLogs.setRequest(true);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(request).trim().getBytes());
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonGenerationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            messageLogsList.add(messageLogs);
            log.info("REQUEST MESSAGE LOG ANDRO to MPROS LOGGING SUCCESS...");
            ESBTokenRequest esbRequest = new ESBTokenRequest();
            esbRequest.setUsername(request.getUsername().trim());
            esbRequest.setImei(request.getImei().trim());
            esbRequest.setTransmissionDateAndTime(request.getTransmissionDateAndTime().trim());
            esbRequest.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber().trim());
            esbRequest.setAuthType(request.getAuthType().trim());
            // Logging to message logs sent message to ESB
            messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("ESB");
            messageLogs.setRequest(true);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(esbRequest).getBytes());
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonGenerationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            messageLogsList.add(messageLogs);
            log.info("REQUEST MESSAGE LOG MPROS to ESB LOGGING SUCCESS...");
            log.info("SEND MESSAGE REQUEST TOKEN TO PROSPERA VIA ESB");
            // access url and get the response
            ESBTokenResponse esbResponse = restClient.tokenRequest(esbRequest);
            response.setResponseCode(esbResponse.getResponseCode());
            response.setResponseMessage(esbResponse.getResponseMessage());
            if (response.getResponseCode().equals(WebGuiConstant.RC_SUCCESS))
                response.setTokenChallenge(esbResponse.getTokenChallenge());
            // Logging to message logs incoming message from ESB
            messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("ESB");
            messageLogs.setRequest(false);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(esbResponse).getBytes());
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonGenerationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            messageLogsList.add(messageLogs);
            log.info("RESPONSE MESSAGE LOG ESB to MPROS LOGGING SUCCESS...");
            // Logging to message logs incoming message from ESB
            messageLogs = new MessageLogs();
            messageLogs.setCreatedDate(new Date());
            messageLogs.setEndpointCode("MPROSPERA");
            messageLogs.setRequest(false);
            messageLogs.setTerminalActivity(terminalActivity);
            try {
                messageLogs.setMessageRaw(new JsonUtils().toJson(response).getBytes());
            } catch (JsonMappingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JsonGenerationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            messageLogsList.add(messageLogs);
            terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogsList);
            log.info("RESPONSE MESSAGE LOG MPROS to ANDRO LOGGING SUCCESS...");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("FAILED....to SAVE Terminal Activity And Message Logs :" + e.getMessage());
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("SAVING terminalActivity or messageLogsList ERROR :" + e.getMessage());
        }
        log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
        return response;
    }

}