package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ParamManagerImplTest {

    @TestConfiguration
    static class ParamManagerImplTestContextConfiguration {
        @Bean
        public ParamManager paramManager() {
            return new ParamManagerImpl();
        }
    }

    @Autowired
    private ParamManager paramManager;

    @MockBean
    private ParamDao paramDao;

    @Before
    public void setUp() {
        SystemParameter parameter = new SystemParameter();
        parameter.setParamName("android.apk.url.download");
        parameter.setParamValue("http://202.158.9.131:8889/mprospera/apk/get-apk?version=3.69");
        User user = new User();
        parameter.setCreatedBy(user);
        Mockito.when(paramDao.findByParamName(parameter.getParamName()))
                .thenReturn(parameter);
    }

    @Test
    public void whenGetByParamName_thenParamShouldBeFound() {
        String paramName = "android.apk.url.download";
        String paramValue = "http://202.158.9.131:8889/mprospera/apk/get-apk?version=3.69";
        SystemParameter found = paramManager.getParamByParamName(paramName);
        assertThat(found.getParamValue()).isEqualTo(paramValue);
    }

}