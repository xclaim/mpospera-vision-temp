package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.LoginTerminalController;
import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.wisma.ScoringMmsPiloting;
import id.co.telkomsigma.btpns.mprospera.request.LoginRequest;
import id.co.telkomsigma.btpns.mprospera.request.ProsperaLoginRequest;
import id.co.telkomsigma.btpns.mprospera.response.LoginResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProsperaLoginResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LoginControllerTest {

    @TestConfiguration
    static class LoginControllerTestContextConfiguration {

        String language = "id";

        @Bean
        public LoginTerminalController loginTerminalController() {
            return new LoginTerminalController();
        }

        @Bean
        protected Locale locale() {
            return new Locale("id", "ID");
        }

    }

    @Autowired
    private LoginTerminalController loginTerminalController;

    @MockBean
    private WSValidationService wsValidationService;

    @MockBean
    private ParameterService parameterService;

    @MockBean
    private TerminalService terminalService;

    @MockBean
    private ProsperaEncryptionService prosperaEncryptionService;

    @MockBean
    private RESTClient restClient;

    @MockBean
    private SentraManager sentraManager;

    @MockBean
    private SafService safService;

    @MockBean
    private UserService userService;

    @MockBean
    private AreaService areaService;

    @MockBean
    private TerminalActivityService terminalActivityService;
    
    @MockBean
    private PilotingService pilotingService;

    final String username = "w0211f";
    String password = "P@ssw0rd";
    String imei = "358525070497940";
    String apkVersion = "3.70";
    String rrn = "51815849045412215005330936140004";
    String transmissionDateTime = "20180517134654";
    String sentraProsperaId = "3243546";
    LoginResponse loginResponse = new LoginResponse();
    Terminal terminal = new Terminal();
    User user = new User();
    Location location = new Location();
    ScoringMmsPiloting piloting = new ScoringMmsPiloting();
    Sentra sentra = new Sentra();
    String roleParam = "role.user";
    String userId = "2423";

    @Before
    public void setUp() {
        ProsperaLoginResponse prosperaLoginResponse = new ProsperaLoginResponse();
        loginResponse.setResponseCode("00");
        when(wsValidationService.loginValidation(username, imei, apkVersion)).thenReturn("00");
        when(terminalService.loadTerminalByImei(imei)).thenReturn(terminal);
        try {
            when(prosperaEncryptionService.encryptField(password)).thenReturn("wgwegwebwery24623qg2eh24h");
        } catch (Exception e) {
            e.printStackTrace();
        }
        prosperaLoginResponse.setResponseCode("00");
        prosperaLoginResponse.setName("JONI ISKANDAR");
        String[] centers = new String[2];
        centers[0] = "13143513";
        centers[1] = "16457457";
        prosperaLoginResponse.setCenters(centers);
        prosperaLoginResponse.setOfficeId("334");
        String[] roles = new String[1];
        roles[0] = "PETUGAS SENTRA";
        prosperaLoginResponse.setRole(roles);
        prosperaLoginResponse.setState("TANGERANG");
        prosperaLoginResponse.setResponseMessage("SUKSES");
        prosperaLoginResponse.setRetrievalReferenceNumber(rrn);
        user.setUserId(Long.parseLong(userId));
        try {
            when(restClient.login(any(ProsperaLoginRequest.class))).thenReturn(prosperaLoginResponse);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        when(parameterService.loadParamByParamName(roleParam, "Petugas Sentra Online")).thenReturn("PETUGAS SENTRA");
        when(sentraManager.findByProsperaId(sentraProsperaId)).thenReturn(sentra);
        when(userService.loadUserByUsername(username)).thenReturn(user);
        when(areaService.findLocationById(user.getOfficeCode())).thenReturn(location);
        when(pilotingService.getActivePilotingOffice(location.getLocationCode())).thenReturn(piloting);
    }

    @Test
    public void WhenDoLogin_ThenReturnResponse() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username);
        loginRequest.setPassword(password);
        loginRequest.setImei(imei);
        loginRequest.setRetrievalReferenceNumber(rrn);
        loginRequest.setTransmissionDateAndTime(transmissionDateTime);
        try {
            LoginResponse result = loginTerminalController.doLogin(loginRequest, apkVersion);
            assertEquals(result.getResponseCode(), loginResponse.getResponseCode());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

}