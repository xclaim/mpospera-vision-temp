package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

public interface SentraManager {

    void save(Sentra sentra);

    void save(List sentra);

    Sentra findByProsperaId(String prosperaId);

    void clearCache();

}