package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.saf.Saf;

import java.util.List;

public interface SafManager {

    List<Saf> findAll();

    void save(Saf saf);

}