package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.dao.TerminalDao;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalManager")
public class TerminalManagerImpl implements TerminalManager {

    @Autowired
    private TerminalDao terminalDao;

    @Cacheable(value = "vsn.terminal.terminalByImei", unless = "#result == null")
    public Terminal getTerminalByImei(String imei) {
        return terminalDao.findByImei(imei);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "vsn.terminal.allTerminal", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "vsn.terminal.terminalByImei", key = "#terminal.imei", beforeInvocation = true),
            @CacheEvict(value = "hwk.terminal.terminalByImei", key = "#terminal.imei", beforeInvocation = true),
            @CacheEvict(value = "hwk.terminal.allTerminal", allEntries = true, beforeInvocation = true)})
    public Terminal insertTerminal(Terminal terminal) {
        terminalDao.save(terminal);
        return terminal;
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "vsn.terminal.allTerminal", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "vsn.terminal.terminalByImei", key = "#terminal.imei", beforeInvocation = true),
            @CacheEvict(value = "hwk.terminal.terminalByImei", key = "#terminal.imei", beforeInvocation = true),
            @CacheEvict(value = "hwk.terminal.allTerminal", allEntries = true, beforeInvocation = true)})
    public Terminal updateTerminal(Terminal terminal) {
        terminalDao.save(terminal);
        return terminal;
    }

    @Override
    @Cacheable(value = "vsn.terminal.allTerminal", unless = "#result == null")
    public List<Terminal> getAllTerminal() {
        return terminalDao.findAll();
    }

    @Override
    @CacheEvict(value = {"vsn.terminal.terminalByImei", "vsn.terminal.allTerminal"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

}