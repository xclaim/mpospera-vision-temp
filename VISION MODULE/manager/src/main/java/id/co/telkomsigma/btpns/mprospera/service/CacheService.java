package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    private SentraManager sentraManager;

    @Autowired
    private TerminalManager terminalManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private ManageApkManager manageApkManager;

    @Autowired
    private LocationManager locationManager;

    @Autowired
    private ParamManager paramManager;

    public void clearCache() {
        sentraManager.clearCache();
        terminalManager.clearCache();
        userManager.clearCache();
        manageApkManager.clearCache();
        locationManager.clearCache();
        paramManager.clearCache();
    }

}