package id.co.telkomsigma.btpns.mprospera.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.wisma.ScoringMmsPiloting;

public interface PilotingDao extends JpaRepository<ScoringMmsPiloting, Long>{
	
	@Query(value = "select s.* from scoring_mms_piloting s where s.office_code = :officecode and s.is_active = '1' ", nativeQuery = true)
	ScoringMmsPiloting findActivePilotingOffice(@Param("officecode") String officeCode);
}
