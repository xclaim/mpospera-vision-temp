package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.PilotingDao;
import id.co.telkomsigma.btpns.mprospera.manager.PilotingManager;
import id.co.telkomsigma.btpns.mprospera.model.wisma.ScoringMmsPiloting;

@Service("pilotingManager")
public class PilotingManagerImpl implements PilotingManager {
	
	@Autowired
	private PilotingDao pilotingDao;

	@Override
	public ScoringMmsPiloting getActivePilotingOffice(String officeCode) {
		return pilotingDao.findActivePilotingOffice(officeCode);
	}

	@Override
	public List<ScoringMmsPiloting> findAll() {
		return pilotingDao.findAll();
	}

}
