package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("areaService")
public class AreaService extends GenericService {

    @Autowired
    private LocationManager locationManager;

    public Location findLocationById(String id) {
        return locationManager.findByLocationId(id);
    }

}