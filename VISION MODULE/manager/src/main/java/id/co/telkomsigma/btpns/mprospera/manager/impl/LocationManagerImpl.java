package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.LocationDao;
import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("locationManager")
public class LocationManagerImpl implements LocationManager {

    @Autowired
    private LocationDao locationDao;

    @Override
    @CacheEvict(value = {"vsn.location.findByLocationId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "vsn.location.findByLocationId", unless = "#result == null")
    public Location findByLocationId(String id) {
        // TODO Auto-generated method stub
        return locationDao.findByLocationId(id);
    }

}